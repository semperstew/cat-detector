from setuptools import find_packages, setup

from cat_detector import __version__

with open("README.md", "r", encoding="utf-8") as f:
    readme = f.read()

DISTNAME = "cat_detector"
DESCRIPTION = ""
MAINTAINER = "Andrew Stewart"
MAINTAINER_EMAIL = "andrew.stewart@elderresearch.com"
URL = "https://gitlab.com/semperstew/cat-detector.git"
LICENSE = ""
DOWNLOAD_URL = URL
INSTALL_REQUIRES = [
    "RPi.GPIO==0.7.0",
    "picamera==1.13",
    "Pillow==7.2.0",
    "requests==2.24.0",
]
TESTS_REQUIRE = [
    #   'pytest=5.4.3'
]
CLASSIFIERS = ["Programming Language :: Python :: 3"]

setup(
    name=DISTNAME,
    maintainer=MAINTAINER,
    maintainer_email=MAINTAINER_EMAIL,
    description=DESCRIPTION,
    license=LICENSE,
    url=URL,
    version=__version__,
    download_url=DOWNLOAD_URL,
    long_description=readme,
    zip_safe=False,  # the package can run out of an .egg file
    classifiers=CLASSIFIERS,
    packages=find_packages(),
    install_requires=INSTALL_REQUIRES,
    tests_require=TESTS_REQUIRE,
)
