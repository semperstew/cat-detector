import logging
import random
from datetime import datetime
from pathlib import Path
import shutil

from flask import Flask, jsonify, redirect, render_template, request, url_for

if logging.getLogger().hasHandlers():
    logging.getLogger().setLevel(logging.INFO)
else:
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s %(levelname)-8s %(message)s"
    )

# ------------
# Globals
# ------------
IMG_UPLOAD_DIR = Path(__file__).parent / "static" / "unlabeled"
IMG_UPLOAD_DIR.mkdir(exist_ok=True, parents=True)

LABELED_IMG_DIR = Path("/data/mldata/cat-detector/")  # yes, a hardcoded a path. sue me.
LABELED_IMG_DIR.mkdir(exist_ok=True, parents=True)

LABELED_IMG_CSV = LABELED_IMG_DIR / "labels.csv"
if not LABELED_IMG_CSV.exists():
    with LABELED_IMG_CSV.open("w") as csv:
        csv.write("image_name,label,label_name\n")

label_names_dict = {0: "Atlas", 1: "Fish", 2: "Both"}


# ------------
# Functions
# ------------
def choose_unlabeled_image():
    image_list = [img.name for img in IMG_UPLOAD_DIR.rglob("*.png")]
    if image_list:
        img_name = f"unlabeled/{random.choice(image_list)}"
    else:
        img_name = "thumbs-up.png"
    return img_name


# ------------
# App Routes
# ------------

app = Flask(__name__)


@app.route("/")
def index():
    img_name = choose_unlabeled_image()
    return render_template("index.html", img_name=img_name)


@app.route("/", methods=["POST"])
def upload_file():
    uploaded_file = request.files["file"]
    filename = f"img-{datetime.utcnow().strftime('%Y%m%d%H%M%S')}.png"
    try:
        uploaded_file.save(IMG_UPLOAD_DIR / filename)
    except Exception as e:
        return jsonify(status="error", message=e), 500
    return jsonify(status="success", message=filename)


@app.route("/label-image", methods=["POST"])
def label_image():
    label_value = int(request.form["label-value"])
    image = IMG_UPLOAD_DIR / request.form["image-name"]

    if label_value == 99:
        image.unlink(missing_ok=True)
        logging.info(f"Deleted {image}")
    elif label_value in label_names_dict:
        new_location = LABELED_IMG_DIR / image.name
        try:
            shutil.copyfile(image, new_location)
            image.unlink()
            label_name = label_names_dict[label_value]
            with LABELED_IMG_CSV.open("a") as csv:
                csv.write(f"{new_location.name},{label_value},{label_name}\n")
            logging.info(f"Labeled '{new_location.name}' as '{label_name}'")
        except OSError as e:
            logging.error(e)
    else:
        logging.warning(f"Unknown label value '{label_value}'.")
    return redirect(url_for("index"))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
