# raspberry pi pinout
MOTION_PIN = 4

# When using the ULN2003A stepper motor driver board
# the order of the GPIO pins below corresponds
# to [IN1, IN2, IN3, IN4] on the driver board
MOTOR_PINS = [12, 16, 20, 21]  # the order matters!!
STEPS_PER_ROTATION = 2048

# image size
IMAGE_SIZE = (256, 256)

# upload server url
SERVER_URL = "http://192.168.1.64:5000"
