import io
import logging
import time
from typing import Optional

import requests
import RPi.GPIO as GPIO
from picamera import PiCamera
from requests.exceptions import ConnectionError

from config import IMAGE_SIZE, MOTION_PIN, SERVER_URL

if logging.getLogger().hasHandlers():
    logging.getLogger().setLevel(logging.INFO)
else:
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s %(levelname)-8s %(message)s"
    )


def take_picture(camera: PiCamera) -> bytes:
    with io.BytesIO() as img_stream:
        camera.capture(img_stream, "jpeg")
        return img_stream.getvalue()


def upload_picture(image: bytes, url: str, image_name: Optional[str] = None) -> None:
    if image_name is None:
        image_name = ""
    files = {"file": (image_name, image, "image/jpeg")}
    try:
        response = requests.post(url, files=files)
    except ConnectionError as e:
        logging.error(e)

    if response.status_code != 200:
        logging.warn(response.json().get("message", "Unhandled server error."))
    else:
        logging.info("New image uploaded.")


def pipeline(camera, upload_url):
    logging.info("Motion detected!!!")
    img = take_picture(camera)
    upload_picture(image=img, url=upload_url)


if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(MOTION_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    camera = PiCamera()
    camera.resolution = IMAGE_SIZE
    time.sleep(1)  # allow the camera to warmup

    try:
        while True:
            GPIO.wait_for_edge(MOTION_PIN, GPIO.RISING)
            pipeline(camera=camera, upload_url=SERVER_URL)
    except KeyboardInterrupt:
        pass
    finally:
        GPIO.cleanup()
