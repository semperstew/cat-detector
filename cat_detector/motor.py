import RPi.GPIO as GPIO
import time
import itertools
from config import MOTOR_PINS, STEPS_PER_ROTATION


def stop_motor() -> None:
    GPIO.output(MOTOR_PINS, GPIO.LOW)


def step_once(step_num: int, clockwise: bool = True) -> None:
    motor_pins = MOTOR_PINS.copy()
    if not clockwise:
        motor_pins.reverse()

    GPIO.output(motor_pins.pop(step_num), GPIO.HIGH)
    GPIO.output(motor_pins, GPIO.LOW)


def rotate(degrees: int, delay_ms: int = 2, **kwargs) -> None:
    # min delay between steps of 28BYJ-48 stepper motor is 2 milliseconds
    delay_ms = max(2, delay_ms)
    num_steps = round(degrees * STEPS_PER_ROTATION / 360)
    pin_cycle = itertools.cycle([0, 1, 2, 3])
    for step in itertools.islice(pin_cycle, num_steps):
        step_once(step_num=step, **kwargs)
        time.sleep(delay_ms / 1000)


if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(MOTOR_PINS, GPIO.OUT)

    try:
        rotate(degrees=360)
        stop_motor()
    except KeyboardInterrupt:
        pass
    finally:
        GPIO.cleanup()
